<?php
//int
$v1 = 10;
$v01 = -10;
$v001 = 0;

//float
$v2 = 10.5;
$v02 = -10.2;

//bool
$v3 = true;
$v03 = false;

//string
$v5 = 'Texto';
$v6 = "Texto\n";

//void

//mixed variavel de varios tipos(int, float, string)

//arrays
$v7 = [];
$v8 = array();//funcao para criar array.

//Objeto
$v9 = new class
{ };
$v10 = (object) [];
$v11 = new stdClass();

//gettype retorna o tipo da variavel
$type = gettype(10.5);

?>