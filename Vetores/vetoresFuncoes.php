<?php

$array = [1, 2, 3];

//ADICIONA UM ELEMENTO AO FINAL
array_push($array, 4);
print_r($array);
echo "\n";

//REMOVE UM ELEMENTO AO FINAL
array_POP($array);
print_r($array);
echo "\n";

//ADICIONA UM ELEMENTO AO INICIO
array_unshift($array, 4);
print_r($array);
echo "\n";

//REMOVE UM ELEMENTO DO INICIO
array_shift($array);
print_r($array);
echo "\n";

//EXIBE O ULTIMO VALOR DO ARRAY
print_r(end($array));
echo "\n";

//AMOSTRA A QUANTIDADE DE VALORES NO ARRAY
print_r(count($array));
echo "\n";

//MAIS FUNCOES PARA ARRAYS NESSE LINK
//https://www.php.net/manual/pt_BR/ref.array.php

?>