<?php

// XOR
// ESSA CONDICAO ATIVA APENAS QUANDO A OU B FOREM VALORES DISTINTOS
// Nesse formato: (boolean || boolean) && (boolean) !== (boolean)
//EXEMPLO:
//(1 == 1 || 2 == 2) && (1 == 1) !== (2 == 2);
//(true || true) && (true) !== (true)
// true && false
// false
//A porta XOR e conhecida por ser EXCLUSIVIDADE, apenas um valor para dar retornar TRUE

$a = (1 == 1 || 2 == 2) && (1 == 1) !== (2 == 2);// false

$b = (1 == 1 || 2 == 1) && (1 == 1) !== (2 == 1);// true

$c = (2 == 1 || 2 == 2) && (2 == 1) !== (2 == 2);// true

$d = (1 == 2 || 2 == 1) && (1 == 2) !== (2 == 1);// false

var_dump($a);
var_dump($b);
var_dump($c);
var_dump($d);

?>